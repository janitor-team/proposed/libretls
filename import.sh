#!/bin/sh
set -eu

input=$1

tar -x -f "${input}" --strip-components 1 \
	--exclude 'libressl-*/*/VERSION' \
	'libressl-*/Makefile.am.common' \
	'libressl-*/VERSION' \
	'libressl-*/configure.ac' \
	'libressl-*/include/Makefile.am' \
	'libressl-*/include/compat/arpa/inet.h' \
	'libressl-*/include/compat/fcntl.h' \
	'libressl-*/include/compat/limits.h' \
	'libressl-*/include/compat/netdb.h' \
	'libressl-*/include/compat/netinet/*.h' \
	'libressl-*/include/compat/pthread.h' \
	'libressl-*/include/compat/stdio.h' \
	'libressl-*/include/compat/stdlib.h' \
	'libressl-*/include/compat/string.h' \
	'libressl-*/include/compat/sys/ioctl.h' \
	'libressl-*/include/compat/sys/mman.h' \
	'libressl-*/include/compat/sys/param.h' \
	'libressl-*/include/compat/sys/socket.h' \
	'libressl-*/include/compat/sys/stat.h' \
	'libressl-*/include/compat/sys/time.h' \
	'libressl-*/include/compat/sys/types.h' \
	'libressl-*/include/compat/time.h' \
	'libressl-*/include/compat/unistd.h' \
	'libressl-*/include/compat/win32netcompat.h' \
	'libressl-*/include/tls.h' \
	'libressl-*/libtls.pc.in' \
	'libressl-*/m4/ax_add_fortify_source.m4' \
	'libressl-*/m4/ax_check_compile_flag.m4' \
	'libressl-*/m4/check-hardening-options.m4' \
	'libressl-*/m4/check-libc.m4' \
	'libressl-*/m4/check-os-options.m4' \
	'libressl-*/m4/disable-compiler-warnings.m4' \
	'libressl-*/man/Makefile.am' \
	'libressl-*/man/tls_*.3' \
	'libressl-*/scripts/wrap-compiler-for-flag-check'

tar -x -f "${input}" --strip-components 2 \
	-s '/VERSION/LIBTLS_VERSION/' \
	'libressl-*/crypto/compat/arc4random*.[ch]' \
	'libressl-*/crypto/compat/bsd-asprintf.c' \
	'libressl-*/crypto/compat/chacha_private.h' \
	'libressl-*/crypto/compat/explicit_bzero*.c' \
	'libressl-*/crypto/compat/freezero.c' \
	'libressl-*/crypto/compat/getentropy*.c' \
	'libressl-*/crypto/compat/getpagesize.c' \
	'libressl-*/crypto/compat/getprogname*.c' \
	'libressl-*/crypto/compat/posix_win.c' \
	'libressl-*/crypto/compat/reallocarray.c' \
	'libressl-*/crypto/compat/strcasecmp.c' \
	'libressl-*/crypto/compat/strlcpy.c' \
	'libressl-*/crypto/compat/strsep.c' \
	'libressl-*/crypto/compat/timegm.c' \
	'libressl-*/crypto/compat/timingsafe_memcmp.c' \
	'libressl-*/tls/*.[ch]' \
	'libressl-*/tls/Makefile.am' \
	'libressl-*/tls/VERSION' \
	'libressl-*/tls/tls.sym'
